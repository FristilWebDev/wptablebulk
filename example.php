<?php
/**
 * Adds custom bulk actions to wp table
 */

//get our file
require_once(realpath(dirname(__FILE__)) . "/WpTableBulk.class.php");

//bulk actions we want to add
$myBulkActions = array(
    /*"bulkname"    => array(               //action call bulk_edit-{bulkname}
        "post_type" => array("post"),       //array of post types to add this bulk
        "name"      => "Export"             //human readable runs trug _e()
    )*/

    "export" => array(                      //action call bulk_edit-export
        "post_type" => array("post"),       //bulk will show in post table
        "name"      => "Export"             //it will say "Export"
    )

    /*,
    "NextBulk" => array(                        //action call bulk_edit-NextBulk
        "post_type" => array("page", "post"),   //bulk will show in page and post table
        "name"      => "Next bulk action"       //it will say "Next bulk action"
    ) */
);


//initiate
new WpTableBulk($myBulkActions);


//when an export bulk is detected do this action
add_action('bulk_edit-export', 'myBulkAction_export');
function myBulkAction_export($post_ids) {
    var_dump($post_ids);
    die;
}


/*
//when an NextBulk bulk is detected do this action
add_action('bulk_edit-NextBulk', 'myBulkAction_NextBulk');
function myBulkAction_NextBulk($post_ids) {
    // some action
}
*/