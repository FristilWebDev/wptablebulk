#WpTableBulk#

Adds an easy way of adding bulk actions to wp tables.

When an bulk action is detected an action is called bulk_edit-{$name}
this makes this future proof since when core support is added it will be an action.
What may differ at that point is whats delivered at that action.

Se example.php for example usage.

Idea from here http://www.skyverge.com/blog/add-custom-bulk-action/ Thanks!


