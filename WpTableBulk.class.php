<?php
/**
 * Adds bulkactions to wp table.
 * calls an action when bulk is detected
 * action: bulk_edit-{$name}
 *
 * There is an non js way of adding bulkactions. but sinze
 * it changes the head even when action is done it dont realy work
 *
 * idea from here http://www.skyverge.com/blog/add-custom-bulk-action/
 *
 */



if (!class_exists(WpTableBulk)) {
    class WpTableBulk {

        private $bulkActions = array();

        /** Constructor
         *
         * @param $bulkActions
         */
        function __construct($bulkActions) {

            //only run in admin
            if (is_admin()) {
                //do we even have any bulks to run?
                if (count($bulkActions) > 0) {

                    //save bulks so we have it when needed
                    $this->bulkActions = $bulkActions;


                    //queue actions
                    add_action('admin_footer-edit.php', array(&$this, "addToDropDown"));
                    add_action('load-edit.php', array(&$this, "checkAndDoAction"));
                }
            }
        }

        /**
         * Add all actions to the bulk action dropdown
         */
        function addToDropDown() {
            global $post_type;
            foreach ($this->bulkActions as $func => $specs) {
                if (in_array($post_type, $specs['post_type'])) {
                    ?>
                    <script type="text/javascript">
                        jQuery(document).ready(function () {
                            jQuery('<option>').val('<?php echo $func ?>').text('<?php _e( $specs['name'])?>').appendTo("select[name='action']");
                            jQuery('<option>').val('<?php echo $func ?>').text('<?php _e( $specs['name'])?>').appendTo("select[name='action2']");
                        });
                    </script>
                <?php
                }
            }
        }

        /**
         * Check if there is an action that matches our list
         */
        function checkAndDoAction() {


            $tableList   = array(
                'WP_Posts_List_Table',
                'WP_Users_List_Table',
                'WP_Comments_List_Table'
            );
            $requestList = array(
                'post',
                'media',
                'ids');


            // check if we have a table with actions
            foreach ($tableList as $tableName) {
                $wp_list_table = _get_list_table($tableName);
                $action        = $wp_list_table->current_action();

                //did we get an action that is in our list?
                if (!empty($action) && array_key_exists($action, $this->bulkActions)) {
                    break;
                }
                else {
                    $action = "";
                }
            }

            //make sure we have an action
            if (empty($action)) {
                return;
            }


            //check if we have any ids
            foreach ($requestList as $listName) {
                if (isset($_REQUEST[$listName])) {
                    $post_ids = array_map('intval', $_REQUEST[$listName]);
                }
                if (!empty($post_ids)) {
                    break;
                }
            }
            //make sure we have ids
            if (empty($post_ids)) {
                return;
            }

            //make sure request gets here from within admin
            check_admin_referer('bulk-posts');


            //do bulk action send an array with ids
            do_action('bulk_edit-' . $action, $post_ids);

            //after action send back to correct place
            global $typenow;

            // this is based on wp-admin/edit.php
            $sendback = remove_query_arg(array('exported', 'untrashed', 'deleted', 'ids'), wp_get_referer());
            if (!$sendback) {
                $sendback = admin_url("edit.php?post_type=$typenow");
            }
            $sendback = add_query_arg('paged', $wp_list_table->get_pagenum(), $sendback);
            $sendback = remove_query_arg(array('action', 'action2', 'tags_input', 'post_author', 'comment_status', 'ping_status', '_status', 'post', 'bulk_edit', 'post_view'),
                                         $sendback
            );

            wp_redirect($sendback);
            exit();

        }

    }
}